# ACF Field Type: Spotify
### A nifty little field that let's you add tracks or artists from Spotify to a post/page. ###

## How does it work?
I'm not sure. But it uses [React](https://facebook.github.io/react/) and the [Spotify Web API](https://developer.spotify.com/web-api/).

## How does one install it?
If i were you, i would go to the [Downloads](https://bitbucket.org/jonasjonasjonas/acf-spotify/downloads)-page and download the latest version as a zip-file, and add it to your plugins-folder.

## What does it return?
It return an array of tracks/artists with data from the Spotify API.

## Can i add more functionality to it if i want?
Yes, there's a [WebPack](https://webpack.github.io/) build system and all source files are included.

## How about support?
ATM it **only supports ACF V5**. As for browser-support, i have no clue. 

### This is how it look when you add a field:
![acf-spotify_add-a-field.png](https://bitbucket.org/repo/4qRxoE/images/1778485105-acf-spotify_add-a-field.png)

### This is how it looks when you fill out a field:
![acf-spotify_fill-a-field.png](https://bitbucket.org/repo/4qRxoE/images/3351406751-acf-spotify_fill-a-field.png)

# License and illegality
I don't know much about the law. If i'm breaking it in any way by letting others use this, please let me know. 
I figured the [MIT](http://choosealicense.com/licenses/mit/) license makes sense - right?

### Remember that by using the Spotify Web API, you agree to the [Developer Terms of Use](https://developer.spotify.com/developer-terms-of-use/) defined by Spotify! ###