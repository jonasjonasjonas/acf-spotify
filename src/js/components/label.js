import React from 'react';

export default class Label extends React.Component {
  render() {
    return (
      <div className="acfsf-label">
        { this.props.label }
      </div>
    )
  }
}
