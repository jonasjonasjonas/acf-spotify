import React from 'react';
import _ from "underscore";

export default class SearchBar extends React.Component {
  constructor() {
    super();
    this.onChangeHandler = _.debounce(this.onChangeHandler, 200)
    this.fauxSearch = this.fauxSearch.bind(this);
    this.get = this.get.bind(this);

    this.state = {
      currentQuery: null,
    };
  }

  get(q) {
    return fetch('https://api.spotify.com/v1/search?limit=3&type=' + this.props.type + '&q=' + q)
    .then(response => response.json())
    .then(json => json);
  }

  componentDidMount() {
    // this.fauxSearch();
  }

  fauxSearch() {
    let event = new Event('input', { bubbles: true });
    this.refs.query.value = "Ke";
    this.refs.query.dispatchEvent(event);
  }

  onChangeHandler() {
    if(this.refs.query.value.length == 0) { this.props.onChange({}); }
    if(this.refs.query.value.length < 2) { return; }
    let queryString = encodeURIComponent(this.refs.query.value);
    this.get(queryString).then(response => {
      let returnValue = this.props.type == 'artist' ? response.artists.items : response.tracks.items;
      this.props.onChange(returnValue);
    });
  }

  render() {
    return (
      <div className="acfsf-search-bar">
        <input type="text" ref="query" onChange={this.onChangeHandler.bind(this)} />
      </div>
    )
  }
}
