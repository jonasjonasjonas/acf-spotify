import React from 'react';
import SingleResult from './single-result.js';

export default class SearchResults extends React.Component {
  constructor() {
    super();
    this.state = {
    };
  }

  render() {
    if(Object.keys(this.props.results).length < 1) {
      return (
        <div className="acfsf-search-results">
          <p>Start typing to get results</p>
        </div>
      )
    }

    if(this.props.results.length < 1) {
      return (
        <div className="acfsf-search-results">
          <p>No results</p>
        </div>
      )
    }

    return (
      <div className="acfsf-search-results">
        {this.props.results.map(result => <SingleResult key={result.id} onAdd={this.props.onAdd} onRemove={this.props.onRemove} item={result} />)}
      </div>
    )

  }
}
