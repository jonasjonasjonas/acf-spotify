import React from 'react';
import _ from 'underscore';

export default class ResultControls extends React.Component {
  render() {
    return (
      <div className="acfsf-result-controls">
        { this.props.isAdd ? <div className="acfsf-button add" onClick={this.props.onAdd}/> : null}
        { this.props.isRemove ? <div className="acfsf-button remove" onClick={this.props.onRemove}/> : null}
      </div>
    )
  }
}
