import React from 'react';
import _ from "underscore";
import ResultControls from './single-result-controls.js';

export default class SingleResult extends React.Component {

  onAddHandler() {
    this.props.onAdd(this.props.item);
  }

  onRemoveHandler() {
    this.props.onRemove(this.props.item);
  }

  getImage() {
    let images = this.props.item.type == 'artist' ? this.props.item.images : this.props.item.album.images;
    return images.length > 0 ? <img src={_.last(images).url} /> : <img src="http://cdn.flaticon.com/png/256/8710.png" />
  }

  render() {
    return (
      <div key={this.props.id} className="acfsf-single-result">
        <div className="acfsf-result-image">
        {this.getImage()}
        </div>
        <div className="acfsf-result-label">
          <p>{this.props.item.name}</p>
        </div>
        <ResultControls
          onAdd={this.onAddHandler.bind(this)}
          onRemove={this.onRemoveHandler.bind(this)}
          isAdd={_.isFunction(this.props.onAdd)}
          isRemove={_.isFunction(this.props.onRemove)} />
      </div>
    )
  }
}
