import './../style/index.scss';

import React from 'react';
import ReactDOM from 'react-dom';

import _ from 'underscore';

import "whatwg-fetch";

import Label from './components/label.js';
import SearchBar from './components/search-bar.js';
import SearchResults from './components/search-results.js';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.valueContainer = document.getElementById(this.props.acfKey + '-value');
    this.renderSearch = this.renderSearch.bind(this);
    this.renderChosen = this.renderChosen.bind(this);

    let currentValue = this.valueContainer.value ? JSON.parse(this.valueContainer.value) : false;

    this.state = {
      chosen: _.isArray(currentValue) ? currentValue : [],
      results: []
    }
  }

  componentDidUpdate() {
    this.valueContainer.value = JSON.stringify(this.state.chosen);
  }

  onAddHandler(element) {
    return this.setState({
      chosen: this.state.chosen.concat([element])
    });
  }

  onRemoveHandler(element) {
    return this.setState({
      chosen: _.without(this.state.chosen, _.findWhere(this.state.chosen, element))
    });
  }

  updateResults(newResults) {
    return this.setState({
      results: newResults
    })
  }

  renderSearch() {
    if(this.state.chosen.length >= this.props.limit) {
      return null;
    }

    let sortedResults = _.filter(this.state.results, item => {
      return _.isUndefined(_.findWhere(this.state.chosen, {id: item.id}));
    })

    return (
      <div>
        <Label label="Search Spotify" />
        <SearchBar onChange={this.updateResults.bind(this)} {...this.props} />
        <SearchResults
          results={sortedResults}
          onAdd={this.onAddHandler.bind(this)} />
      </div>
    )
  }

  renderChosen() {

    if(this.state.chosen.length < 1) {
      return null;
    }

    let labelText = "Chosen ";
        labelText += this.props.type;
        labelText += this.state.chosen.length > 1 ? "s" : '';
        labelText += ': ' + this.state.chosen.length + '/' + this.props.limit;

    return (
      <div>
        <Label label={labelText}/>
        <SearchResults
          results={this.state.chosen}
          onRemove={this.onRemoveHandler.bind(this)} />
      </div>
    )
  }

  render() {
    return (
      <div className="acf-spotify-field">
        {this.renderSearch()}
        {this.renderChosen()}
      </div>
    );
  }
}

let mountPoints = document.getElementsByClassName('mount-point');
if(mountPoints.length > 0) {
  [].forEach.call( mountPoints, (mt) => {
    ReactDOM.render( <App {...mt.dataset} />, mt )
  });
}
