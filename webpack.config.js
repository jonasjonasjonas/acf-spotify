var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: [
    'babel-polyfill',
    path.resolve(__dirname, 'src/js/index.js'),
  ],
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: 'js/bundle.js'
  },
  module: {
    loaders: [{
      include: [
        path.resolve(__dirname, "src"),
      ],
      test: /\.js?$/,
      loader: 'babel',
      query: {
        presets: ['es2015','react']
      }
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract("style", 'css!autoprefixer!sass')
    }, {
      test: /\.(png|jpg|svg)$/,
      loader: 'url-loader?limit=8192'
    }]
  },
  plugins: [
      new ExtractTextPlugin("css/bundle.css"),
  ]
};
