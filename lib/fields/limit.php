<?php
acf_render_field_setting( $field, array(
    'label'         => __('Limit','acf-spotify'),
    'instructions'  => __('Maximum number of tracks/artist that can be added','acf-spotify'),
    'type'          => 'number',
    'name'          => 'spotify_limit'
));
