<?php
acf_render_field_setting( $field, array(
    'label'         => __('Type','acf-spotify'),
    'instructions'  => __('Choose between artists and tracks','acf-spotify'),
    'type'          => 'radio',
    'name'          => 'spotify_type',
    'layout'        => 'horizontal',
    'class'         => 'spotify-type-select',
    'choices'       => array(
        'artist'          => __('Artist', 'acf-spotify'),
        'track'           => __('Track', 'acf-spotify')
    )
));
