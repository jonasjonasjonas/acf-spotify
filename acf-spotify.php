<?php

/*
Plugin Name: Advanced Custom Fields: Spotify
Plugin URI: PLUGIN_URL
Description: SHORT_DESCRIPTION
Version: 1.0.0
Author: Jonas E. Thomsen
Author URI: http://jonasjonasjonas.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/




// 1. set text domain
// Reference: https://codex.wordpress.org/Function_Reference/load_plugin_textdomain
load_plugin_textdomain( 'acf-spotify', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );




// 2. Include field type for ACF5
// $version = 5 and can be ignored until ACF6 exists
function include_field_types_spotify( $version ) {

	include_once('lib/acf-spotify-v5.php');

}

add_action('acf/include_field_types', 'include_field_types_spotify');




// 3. Include field type for ACF4
function register_fields_spotify() {

	include_once('lib/acf-spotify-v4.php');

}

add_action('acf/register_fields', 'register_fields_spotify');




?>
